//
//  main.c
//  ex2
//
//  Created by 이재현 on 2018. 10. 17..
//  Copyright © 2018년 이재현. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXLINE 1000

void eval(char *cmdline) {
    char *cmd = NULL;
    cmd = strtok(cmdline, " \n");
    
    if(cmd == NULL || strlen(cmd) == 0) {
        return;
    } else if(strcmp(cmd, "exit") == 0) {
        char *num = NULL;
        num = strtok(NULL, " \n");
        fprintf(stderr, "exit\n");
        if(num == NULL || strlen(num) == 0) {
            exit(0);
        } else {
            int n = atoi(num);
            exit(n);
        }
    } else if(strcmp(cmd, "cd") == 0) {
        char *dir = strtok(NULL, " \n");
        chdir(dir);
        return;
    } else if(strcmp(cmd, "pwd") == 0) {
        char pwd[MAXLINE];
        getcwd(pwd, MAXLINE);
        printf("%s\n", pwd);
        return;
    } else if(strcmp(cmd, "rm") == 0) {
        char *file = strtok(NULL, " \n");
        unlink(file);
        return;
    } else if(strcmp(cmd, "mv") == 0) {
        char *file1 = strtok(NULL, " \n");
        char *file2 = strtok(NULL, " \n");
        rename(file1, file2);
        return;
    } else if(strcmp(cmd, "cp") == 0) {
        char *sfname, *dfname;
        int s, d, n;
        char buf[MAXLINE];
        
        sfname = strtok(NULL, " \n");
        dfname = strtok(NULL, " \n");
        
        s = open(sfname, O_RDONLY);
        
        if(s < 0) {
            printf("swsh: No such file\n");
            return;
        }
        
        d = open(dfname, O_WRONLY|O_CREAT, 0777);
        
        while(n = read(s, buf, MAXLINE)) {
            write(d, buf, n);
        }
        
        close(s);
        close(d);
        
        return;
    } else if(strcmp(cmd, "cat") == 0) {
        char *fname;
        
        int fd;
        int n;
        
        char buf[MAXLINE];
        
        fname = strtok(NULL, " \n");
        
        fd = open(fname, O_RDONLY);
        
        if(fd < 0) {
            gets(buf);
            printf("%s\n", buf);
        } else {
            while(n = read(fd, buf, MAXLINE)) {
                write(1, buf, n);
            }
            
            close(fd);
        }
        
        
        
        return;
    } else {
        printf("swsh: command not found: %s\n", cmd);
        return;
    }
}

int main() {
    char cmdline[MAXLINE];
    
    signal(SIGINT, SIG_IGN);
    signal(SIGTSTP, SIG_IGN);
    
    while(1) {
        printf("> ");
        fgets(cmdline, MAXLINE, stdin);
        if(feof(stdin)) {
            exit(0);
        }
        
        eval(cmdline);
    }
}
